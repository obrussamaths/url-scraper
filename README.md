# url-scraper #
--------------

Simple node module to get the title and description from a URL.

Give it a URL string and it returns an object with title and description properties.

For a title it looks for the site title and then falls back to h1 or h2 tag. 
For a description it looks for the meta description tag then falls back to the first paragraph.  It takes the first sentence or line only.

Example usage:

```
    var scraper = require('url-scraper');

    var url = 'http://www.bbc.co.uk/education';
    scraper.scrape(url, function (err, json) {
        if (err) {
            console.log(err);
        } else {
            console.log(json);
        }
    });
```
Output:
```
{
    "title": "BBC Bitesize - Home",
    "description": "Learning resources for adults, children, parents and teachers: find videos and audio clips by level, subject and topic",
    "url": "http://www.bbc.co.uk/education"
}
```

It also allows to set a blacklist, for certain titles to be ignored.
```
// Set the title blacklist of websites that have data that should not be returned as it is not informative enough
scraper.setTitleBlacklist([
    'Login | HegartyMaths',
    'Login'
]);
```

Some web pages may have undescriptive meta tag descriptions. In these cases we can tell Scraper to fall back to the first paragraph for the description. 
```
// Set the descriptions that are un-informative and should force a fallback to the first paragraph instead
scraper.setDescriptionFallbacks(['dc:description from xml asset']);
```                