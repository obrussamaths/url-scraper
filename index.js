var request = require('request');
var cheerio = require('cheerio');

// Website title blacklist that are not returned by the scraper
var titleBlacklist = [];
// Descriptions that if encountered force the scraper to fallback to scraping the first paragraph
var descriptionFallbacks = [];

module.exports = {

    /**
     * Given a URL, scrapes the website to return its title and description.
     * @param url - the URL to scrape.
     * @param callback - will be invoked with an object containing the site's title and description.
     */
    scrape: function (url, callback) {

        request({ url: url, timeout: 15000}, function(err, response, html) {
            if (err) {
                callback(err);
            } else {
                if (response.statusCode == 200) {               
                    var $ = cheerio.load(html);

                    var meta = {};
                    $('meta').each(function(i, tag){
                        var key   = $(this).attr('name');
                        var value = $(this).attr('content');
                        if (key && value) meta[key] = value;
                    });

                    var title = $('title').first().text() || $('h1').first().text() || $('h2').first().text(); 

                    // Consider a pipe as a terminating character in the title
                    title = title.split('|')[0];

                    // Trim any additional whitespace
                    title = title.trim();

                    // Check if website data should be returned
                    if (titleBlacklist.indexOf(title) === -1) {
                        // Get meta description, or fall back to first paragraph
                        var description = (function (desc) {
                            if (!desc || descriptionFallbacks.indexOf(desc) !== -1) {
                                // Only get the text of the first paragraph
                                return $('p').first().text();
                            } else {
                                return desc;
                            }
                        })(meta['description']);

                        // Format the title
                        title = escapeSpecialCharacters(title);

                        // Format the description
                        // Replace all contiguous whitespace with single spaces,
                        description = description.replace(/\s\s+/g, ' ');

                        // Consider punctation as a terminating character
                        if (description.indexOf('.') !== -1) {
                            description = description.split('.')[0];
                            description = description + '.';
                        } else if (description.indexOf('!') !== -1) {
                            description = description.split('!')[0];
                            description = description + '!';
                        } else if (description.indexOf('?') !== -1) {
                            description = description.split('?')[0];
                            description = description + '?';
                        }

                        // Escape any special characters
                        description = escapeSpecialCharacters(description);
                        
                        // Generate and return the scraped data
                        var json = { title : title, description : description, url : url };
                        callback(null, json);
                    } else {
                        callback(new Error ('%s has a title on the blacklist', url));
                    }
                } else {
                    callback(new Error ('%s returned status %s, not processing', url, response.statusCode));
                }
            }
        });
    },

    /**
     * Set the title blacklist, website data that is not returned by the scraper.
     * @param titles - the title blacklist.
     */
    setTitleBlacklist: function (titles) {
        titleBlacklist = titles;
    },

    /**
     * Set the descriptions that if encountered force the scraper to fallback to scraping the first paragraph.
     * @param descriptions - the fallback descriptions.
     */
    setDescriptionFallbacks: function (descriptions) {
        descriptionFallbacks = descriptions;
    }

};

function escapeSpecialCharacters(string) {
    // Escape all occurrences of the escape character
    var formattedString = string.replace(/\\/g, '\\\\');
    // Escape quotes
    formattedString = formattedString.replace(/'/g, "\\'");

    return formattedString;
}
